# mysetup.py
from distutils.core import setup
import py2exe


from distutils.core import setup
import py2exe
setup(
    windows=[
        {
            'script': 'check.py',
            "icon_resources": [(1, 'main.ico')],
        }
    ],
    options={
        "py2exe":
            {
                "dll_excludes": ["MSVCP90.dll"],
                'includes': ['decimal',],
                "bundle_files": 1,
                "packages": ['wx.lib.pubsub', ]
            }
    },

    data_files=[
        (
            '', ['logging.conf', 'time.bat', ]
        )
    ],
)
