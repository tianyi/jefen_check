# -*- coding: utf-8 -*-

from Controller import BaseController
from utils.ERROR_CODE import TYPE_ERROR
from utils.MyException import MyException
from utils.InitDBSQLite3 import InitDBSQLite3
import simplejson
from settings import LOGIN_VALIDATOIN_URL

# use HTTP
from utils.MultipartPostHandler  import MultipartPostHandler
import urllib2
import cookielib


class LoginController(BaseController):

    def __init__(self, loginModel):
        BaseController.__init__(self)
        self.__model = loginModel
        cookies = cookielib.CookieJar()
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies), MultipartPostHandler)

    def InitSQLite3(self, shopID, shopName, userID, statu):
        InitDBSQLite3(shopID, shopName, userID, statu)

    def ValidateLogin(self, userID, passwd):

        # import pdb; pdb.set_trace()

        # sql = "exec VerifyLogin 'p043','123456'"
        # sql = "exec VerifyLogin '%s','%s'" % (userID, passwd) 
        # re = self.__model.odbc_ms.ExecQuery(sql)
        # if not isinstance(re, list):
        #     raise MyException(TYPE_ERROR[0], TYPE_ERROR[1] % ('list', type(re)))

        params = {
            "username": userID,
            "password": passwd,
        }
        try:
            re = self.opener.open(LOGIN_VALIDATOIN_URL, params)
            data = re.read()
            data_json = simplejson.loads(data)
        except:
            return {"status": -1}
       

        return data_json

    def getModel(self):
        return self.__model
