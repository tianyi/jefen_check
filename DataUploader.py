# -*- coding: utf-8 -*

from settings import EMPLOYEE_NO_BODY, TIME_TO_UPLOAD, LOCAL_DATA_EXPIRE
# from model.Model import BaseModel
from datetime import datetime
from wx.lib.pubsub import setupkwargs
from wx.lib.pubsub import pub
import time
from threading import Thread, Timer
import wx
import Queue
from settings import DATABASES_SQLITE3, DATABASES_MS, UPLOAD_PASSWORK, \
                     UPLOAD_USER_NAME, UPLOAD_PIC_URL_IN, UPLOAD_PIC_URL_OUT, PHOTO_FILE_NAME_UPLOAD
import logging
from utils.Database import DBSQLite, ODBC_MS
from utils.utils import getDateTimeNow, stringToDatetime
import simplejson
# use HTTP upload pic
from utils.MultipartPostHandler import MultipartPostHandler
import urllib2
import cookielib


# class DataUploader(BaseModel):
class DataUploader(Thread):

    def __init__(self, checkController):
        Thread.__init__(self)
        self.logger = logging.getLogger('main.DataUploader')
        self.checkController = checkController
        # self.ldb = DBSQLite(DATABASES_SQLITE3['DB_FILE_NAME'])
        # self.odbc_ms = ODBC_MS(
        #     DATABASES_MS['DRIVER'],
        #     DATABASES_MS['SERVER'],
        #     DATABASES_MS['DATABASE'],
        #     DATABASES_MS['UID'],
        #     DATABASES_MS['PWD']
        # )
        # self.logger.info('create upload thread!')

        '''
            taskQueue: 同步队列, 用来确定是否上传数据
            taskQueue.put(object, block):
            taskQueue.get(block):
                object(int): put item 1
                block: 1 block 0 do not block
        '''
        self.__cleanSQLite()

        self.taskQueue = Queue.Queue(maxsize=2)
        cookies = cookielib.CookieJar()
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies), MultipartPostHandler)
        # self.lock = threading.Lock()
        pub.subscribe(self.__manualUploadHandler, "manualUpload")

        # do not why , Class Timer only run once at windows, not test in linux and Mac
        # Implement myself Timer
        # self.timer = Timer(1, self.__autoUploadHandler)
        self.timer = MyTimer(TIME_TO_UPLOAD, self)
        self.timer.setDaemon(True)
        self.timer.start()

    def __cleanSQLite(self):
        # import pdb;pdb.set_trace()
        sql = "delete from check_%s where check%s_time < datetime('now', '-%s day')"
        sql_in = sql % ('in', 'in', LOCAL_DATA_EXPIRE)
        sql_out = sql % ('out', 'out', LOCAL_DATA_EXPIRE)
        # self.logger.debug('sql_in=' + str(sql_in))
        # self.logger.debug('sql_out=' + str(sql_out))
        # import pdb; pdb.set_trace()
        ldb = DBSQLite(DATABASES_SQLITE3['DB_FILE_NAME'])
        self.logger.debug('try to clean data before '+ str(LOCAL_DATA_EXPIRE) + 'days.')
        self.logger.debug('start deleting checkin data...')
        ldb.ExecNoQuery(sql_in, ())
        self.logger.debug('end deleting checkin data.')
        self.logger.debug('start deleting checkout data...')
        ldb.ExecNoQuery(sql_out, ())
        self.logger.debug('end deleting checkout data.')
        ldb.commit()
        
    def run(self):
        """Run Worker Thread."""
        # self.timer.start()
        while(1):
            self.taskQueue.get(1)
            msg = {'statu': 0, 'msg': u'数据上传中...'}
            pub.sendMessage("autoUpload", msg=msg)
            # import pdb; pdb.set_trace()
            status = self.__uploadData()
            # status = -1
            t = getDateTimeNow()
            if(status == 1):
                msg = {'statu': 1, 'msg': u'上传完成: ' + t}
            elif(status == -1):
                msg = {'statu': -1, 'msg': u'上传失败, 稍后重试'}
            pub.sendMessage("autoUpload", msg=msg)
            
    # 快速响应，否则，函数将阻塞GUI运行，形成假死
    def __manualUploadHandler(self):
        try:
            self.taskQueue.put(2, 0)
        except:
            pass

    def autoUploadHandler(self):
        # print 'tick'
        # self.taskQueue.put(1, 0)
        try:
            self.taskQueue.put(1, 0)
            # pass
        except:
            pass

    # 1: suc -1: fail
    def __uploadData(self):
        # self.lock.acquire()
        
        self.ldb = DBSQLite(DATABASES_SQLITE3['DB_FILE_NAME'])
        self.odbc_ms = ODBC_MS(
            DATABASES_MS['DRIVER'],
            DATABASES_MS['SERVER'],
            DATABASES_MS['DATABASE'],
            DATABASES_MS['UID'],
            DATABASES_MS['PWD']
        )
        self.logger.debug('start upload data')
        # print 'start Upload!'
        # time.sleep(5)
        # import pdb; pdb.set_trace()
        status_checkin = self.__uploadCheckin()
        self.logger.debug('upload check in: status=' + str(status_checkin))
        # print 'upload check in'
        status_checkout = self.__uploadCheckout()
        self.logger.debug('upload check out: status=' + str(status_checkout))
        if(status_checkin == 1 and status_checkin == 1):
            return 1
        else:
            return -1
        # print 'upload check out'
        # print sql
        self.logger.debug('Upload Done!')


    # 1: suc -1: fail
    def __uploadCheckin(self):


        sql = 'select count(*) from check_in where isUploaded=?'


        # import pdb; pdb.set_trace()
        re = self.ldb.ExecQuery(sql, (False, ))

        if((not re) or re[0][0] < 1):
            # self.logger.debug('no data need to upload: check in')
            return 1

        status = 1
        
        # upload check_in data
        sql = "select checkin_time, employee_id, check_pic, id from check_in where isUploaded=?"
        re = self.ldb.ExecQuery(sql, (False, ))

        # sql = 'insert into check_in_test values (?, ?, ?, ?)'
        # parameters = [(r[0], r[1], False, r[2]) for r in re]
        user_id = self.checkController.getModel().getUserID()
        user_id = user_id.encode('utf-8')
        try:
            for r in re:
                binPic = r[2]
                oid = r[3]
                # tmp = open('AFEONDL.jpg', 'wb')
                with open(PHOTO_FILE_NAME_UPLOAD, 'wb') as f:
                    f.write(binPic)
                checkin_time = r[0]
                # import pdb; pdb.set_trace()
                dt = stringToDatetime(checkin_time)
                check_day = dt.date().strftime('%Y-%m-%d')
                check_time = dt.time().strftime('%H:%M:%S')
                pic = open(PHOTO_FILE_NAME_UPLOAD, 'rb')
                employee_id = str(r[1])
                self.__http_uploader(pic, check_day, employee_id, check_time, UPLOAD_PIC_URL_IN, user_id)
                # 1 suc 0: fail -1: someting wrong -2: HTTP error
                data_from_server = self.__http_uploader(pic, check_day, employee_id, check_time, UPLOAD_PIC_URL_IN, user_id)
                if(data_from_server == 1):
                    self.__update_uploaded_status("check_in", oid)
                else:
                    status = -1
                # print x
        except Exception as e:
            self.logger.error('upload check in data error!')
            self.logger.error(e)
            status = -1
            
        # if(parameters):
        #     try:
        #         self.odbc_ms.ExecInsertMany(sql, parameters)
        #     except Exception as e:
        #         self.logger.error('upload check in data error!')
        #         return -1

        # sql = 'update check_in set isUploaded=? where isUploaded=?'
        # self.ldb.ExecNoQuery(sql, (True, False))
        # self.ldb.commit()
        # self.logger.debug('upload check in data end')
        # sql = "delete from check_in where isUploaded=1 and checkin_time < datetime('now', 'start of day', '-? day')"
        return status

    # 1: suc -1: fail
    def __uploadCheckout(self):

        sql = 'select count(*) from check_out where isUploaded=?'
        re = self.ldb.ExecQuery(sql, (False, ))

        if((not re) or re[0][0] < 1):
            # self.logger.debug('no data need to upload: check out')
            return 1
        status = 1
        # upload check_in data
        sql = "select checkout_time, employee_id, check_pic, id from check_out where isUploaded=?"

        re = self.ldb.ExecQuery(sql, (False, ))

        # sql = 'insert into check_out_test values (?, ?, ?, ?)'
        
        # parameters = [(r[0], r[1], False, r[2]) for r in re]
        # import pdb; pdb.set_trace()
        # user_id 操作员编号
        user_id = self.checkController.getModel().getUserID()
        user_id = user_id.encode('utf-8')
        try:
            for r in re:
                binPic = r[2]
                oid = r[3]
                # tmp = open('AFEONDL.jpg', 'wb')
                with open(PHOTO_FILE_NAME_UPLOAD, 'wb') as f:
                    f.write(binPic)
                checkout_time = r[0]
                dt = stringToDatetime(checkout_time)
                check_day = dt.date().strftime('%Y-%m-%d')
                check_time = dt.time().strftime('%H:%M:%S')
                pic = open(PHOTO_FILE_NAME_UPLOAD, 'rb')
                employee_id = str(r[1])
                # 1 suc 0: fail -1: someting wrong -2: HTTP error
                data_from_server = self.__http_uploader(pic, check_day, employee_id, check_time, UPLOAD_PIC_URL_OUT, user_id)
                if(data_from_server == 1):
                    self.__update_uploaded_status("check_out", oid)
                else:
                    status = -1
        except Exception as e:
            self.logger.error('upload check out data error!')
            self.logger.error(e)
            status = -1
            
        # if(parameters):
        #     try:
        #         self.odbc_ms.ExecInsertMany(sql, parameters)
        #     except Exception as e:
        #         self.logger.error('upload check out data error!')
        #         return -1
        # sql = 'update check_out set isUploaded=? where isUploaded=?'
        # self.ldb.ExecNoQuery(sql, (True, False))
        # self.ldb.commit()
        # self.logger.debug('upload check out data end')
        # sql = "delete from check_in where isUploaded=1 and checkin_time < datetime('now', 'start of day', '-? day')"
        return status

    # t_name: table name check_out or check_in
    # oid   : try to upload row id.
    def __update_uploaded_status(self, t_name, oid):
        sql = 'update %s set isUploaded=%s where id=%s '
        sql_r = sql % (t_name, 1, oid)
        self.ldb.ExecNoQuery(sql_r, ())
        self.ldb.commit()
        # self.logger.debug('upload ' + t_name + ' data end: id=' + str(oid))

    # 1 suc 0: fail -1: someting wrong -2: HTTP error
    def __http_uploader(self, binPic, check_day, employee_id, check_time, api_url, shop_id):

        # employee_id=employee
        # check_time=checkin_time
        # check_day=check_day
        # import pdb; pdb.set_trace()

        # params = {"username": "tianyi", "password": "zxcvbnm", "image_path": open("D:/test.jpg", "rb")}
        params = {
            "username": UPLOAD_USER_NAME,
            "password": UPLOAD_PASSWORK,
            "image_path": binPic,
            "check_day": check_day,
            "employee_id": employee_id,
            "check_time": check_time,
            # shop_id: 操作员编号
            "shop_id": shop_id,
        }
        xy = self.opener.open(api_url, params)

        try:
            if(xy.code == 200):
                data = xy.read()
                data1 = simplejson.loads(data)
                return data1["status"]
        except Exception as e:
            self.logger.error(e)
            self.logger.error('upload check out data end')
            return -2
        self.logger.error("something wrong. code=" + str(xy.code) + " msg=" + str(xy.msg))
        return -2


class MyTimer(Thread):

    def __init__(self, time_interval, o):
        Thread.__init__(self)
        self.__time_interval = time_interval
        self.__o = o

    def run(self):
        while 1:
            self.__o.autoUploadHandler()
            time.sleep(self.__time_interval)

if __name__ == "__main__":
    du = DataUploader()
    du.uploadData()