# -*- coding: utf-8 -*

from settings import EMPLOYEE_NO_BODY
from Model import BaseModel
from datetime import datetime
from utils.SQLs import SELECT_EMPLOYEE, UPDATE_EMPLOYEE_INFO_TIME, INSERT_EMPLOYEE
import logging
from utils.utils import stringToDatetime, getDateTimeNow


class EmployeeModel():

    # li = [(employee_id, employee_name), ...]
    def __init__(self, refreshTime, employeeMap):
        self.__refreshTime = refreshTime
        self.__employeeIDList = [i[0] for i in employeeMap]
        self.__employeeNameList = [i[1] for i in employeeMap]
        
    def getEmployeeByIndex(self, index):
        return (self.__employeeIDList[index], self.__employeeNameList[index])

    def getEmployeeNameList(self):
        return self.__employeeNameList

    def getRefreshTime(self):
        return self.__refreshTime

    def setRefreshTime(self, dateTime):
        self.__refreshTime = dateTime
