# -*- coding: utf-8 -*
from utils.Database import DBSQLite
from settings import DATABASES_SQLITE3
from utils.InitDBSQLite3 import InitDBSQLite3
from Model import BaseModel


class LoginModel(BaseModel):

    def __init__(self):
        BaseModel.__init__(self)
        self.__shopID = None
        
    def getShopID(self):
        return self.__shopID

    def setShopID(self, shopID):
        self.__shopID = shopID