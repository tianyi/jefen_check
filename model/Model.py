# -*- coding: utf-8 -*
from utils.Database import DBSQLite, ODBC_MS
from settings import DATABASES_SQLITE3, DATABASES_MS
import logging


class BaseModel():

    def __init__(self):
        self.logger = logging.getLogger('main.BaseModel')
        self.ldb = DBSQLite(DATABASES_SQLITE3['DB_FILE_NAME'])
        self.odbc_ms = ODBC_MS(
            DATABASES_MS['DRIVER'],
            DATABASES_MS['SERVER'],
            DATABASES_MS['DATABASE'],
            DATABASES_MS['UID'],
            DATABASES_MS['PWD']
        )
