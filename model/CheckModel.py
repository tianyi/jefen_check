# -*- coding: utf-8 -*
from Model import BaseModel
import traceback
from utils.MyException import MyException
from utils.ERROR_CODE import SHOP_ERROR, CHECT_OUT_ERROR, GENERATE_MODEL_ERROR
from settings import EMPLOYEE_NO_BODY, PHOTO_FILE_NAME_AFTER, GET_EMPLOYEEIFNO_URL
from EmployeeModel import EmployeeModel
from datetime import datetime
from utils.PicHandler import PicHandler
from utils.utils import getUUID, getDateTimeNow, stringToDatetime
import sqlite3
from utils.SQLs import *
# use HTTP
from utils.MultipartPostHandler import MultipartPostHandler
import urllib2
import cookielib
import simplejson


class CheckModel(BaseModel):

    def __init__(self):
        BaseModel.__init__(self)
        self.__shopID = None
        self.__userID = None
        self.__shopName = ''
        self.__showRM = None
        # self.__employeeIDList = None
        # self.__employeeNameList = None
        self.__employeeList = None
        self.__refreshTime = None   
        cookies = cookielib.CookieJar()
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies), MultipartPostHandler)     
        self.__picHandler = PicHandler()
        self.__InitShopInfo()
        # every time restart client, sync employee info auto
        self.sync()
        self.__InitEmployeeList()
        self.__getRefreshTime()

    def getShopID(self):
        return self.__shopID

    def getUserID(self):
        return self.__userID

    # remember: everytime you need update isShowRM in database and memory
    def updateShowRM(self, isShowRM):
        sql = UPDATE_SHOW_RM
        parameters = (isShowRM, )
        try:
            self.ldb.ExecNoQuery(sql, parameters)
            self.__showRM = isShowRM
        except Exception as e:
            self.logger.error('save show rm error. isShowRM=' + str(isShowRM))
        self.ldb.commit()
        self.__InitEmployeeList()

    def __InitEmployeeList(self):
        em = self.ldb.ExecQuery(SELECT_EMPLOYEE, ())
        rm = []
        if(self.__showRM == 1):  
            rm = self.ldb.ExecQuery(SELECT_REGIONAL_MANAGER, ())
        # (999999, "", 0) = (id, "名字", 是否RM)
        # self.__employeeIDList = [999999] + [i[0] for i in re] + [i[0] for i in rm]
        # self.__employeeNameList = [u'请选择'] + [i[1] for i in re] + [i[1] for i in rm]
        self.__employeeList =  [(999999, u'请选择', 0)] + [(i[0], i[1], 0) for i in em] + [(i[0], i[1], 1) for i in rm]
        
    def __getRefreshTime(self):
        sql = 'select employeeInfoUpdateTime from status'
        re = self.ldb.ExecQuery(sql, ())
        # print re
        # import pdb; pdb.set_trace()
        self.__refreshTime = stringToDatetime(re[0][0])
      
    def __InitShopInfo(self):
        sql = SELECT_SHOP
        re = self.ldb.ExecQuery(sql, ())
        self.__shopID = re[0][0]
        self.__shopName = re[0][1]
        self.__showRM = re[0][2]
        self.__userID = re[0][3]

    def getEmployeeByIndex(self, index):
        # return (self.__employeeIDList[index], self.__employeeNameList[index])
        return self.__employeeList[index]

    def getEmployeeList(self):
        # return self.__employeeNameList
        return self.__employeeList

    def getShopName(self):
        return self.__shopName

    def getRefreshTime(self):
        return self.__refreshTime

    def sync(self):
        # sql = "exec GetEmployeeList 'bj001'"
        # import pdb; pdb.set_trace()

        params = {
            "shop_id": self.__shopID,
        }
        try:
            re = self.opener.open(GET_EMPLOYEEIFNO_URL, params)
            data = re.read()
            data_dic = simplejson.loads(data)
        except Exception as e:
            self.logger.error('Sync employee fail: username=' + self.__shopID)
            return

        if(data_dic["status"] == 1):
            sql1 = 'delete from employee'
            sql2 = 'delete from regional_manager'
            self.ldb.ExecQuery(sql1, ())
            self.ldb.ExecQuery(sql2, ())
            # import pdb; pdb.set_trace()
            for em in data_dic["data"]:
                # print r
                # "name": i.name,
                # "status": i.status,
                # "employee_id": i.employee_id,
                # "shop_id": i.shop_id,
                if(em["employee_type"] == 0):
                    self.ldb.ExecQuery(INSERT_EMPLOYEE, (em["employee_id"], em["name"]))
                elif(em["employee_type"] == 1):
                    self.ldb.ExecQuery(INSERT_REGIONAL_MANAGER, (em["employee_id"], em["name"]))
                else:
                    self.logger.error('save employee, something error:' + str(em))
            sql = UPDATE_EMPLOYEE_INFO_TIME
            t = getDateTimeNow()
            self.ldb.ExecQuery(sql, (t, ))
            self.ldb.commit()
            self.__refreshTime = t
            self.logger.info('get employee suc. employee num=' + str(len(data_dic["data"])))
        else:
            self.logger.error('get employee info fail, get Null info')
            self.logger.error('data_dic=' + str(data_dic))
        self.__InitEmployeeList()
        # sql = "exec GetEmployeeList '%s'" % (self.__shopID)

    def ExistNotUploadData(self):
        sql_in = 'select count(*) from check_in where isUploaded=0'
        sql_out = 'select count(*) from check_out where isUploaded=0'
        re_in = self.ldb.ExecQuery(sql_in, ())
        re_out = self.ldb.ExecQuery(sql_out, ())

        if(re_in[0][0] > 0 or re_out[0][0] > 0):
            return True
        else:
            return False

    # statu 1 : 保存成功
    # statu -1：保存失败
    # statu 0
    def saveCheckin(self, employ_id):
        parameters = None
        pic_raw = None
        statu = 0
        self.__picHandler.do()
        with open(PHOTO_FILE_NAME_AFTER, 'rb') as f:
            pic_raw = sqlite3.Binary(f.read())
        dateTime = getDateTimeNow()
        sql = INSERT_CHECK_IN
        # uuid = getUUID()
        parameters = (None, dateTime, employ_id, False, pic_raw)
        try:
            self.ldb.ExecNoQuery(sql, parameters)
            statu = 1
        except Exception as e:
            statu = -1
            self.logger.error('save check in data fail. employ_id=' + str(employ_id))
        self.ldb.commit()
        return statu

    # statu 1 : 保存成功
    # statu -1：保存失败
    # statu 0
    def saveCheckout(self, employ_id):
        parameters = None
        pic_raw = None
        statu = 0
        self.__picHandler.do()
        with open(PHOTO_FILE_NAME_AFTER, 'rb') as f:
            pic_raw = sqlite3.Binary(f.read())
        dateTime = getDateTimeNow()
        sql = INSERT_CHECK_OUT
        # uuid = getUUID()
        parameters = (None, dateTime, employ_id, False, pic_raw)
        try:
            self.ldb.ExecNoQuery(sql, parameters)
            statu = 1
        except Exception as e:
            statu = -1
            self.logger.error('save check in data fail. employ_id=' + str(employ_id))
        self.ldb.commit()
        return statu

if __name__ == '__main__':
    pass

