# -*- coding: utf-8 -*-
from utils.Database import DBSQLite, ODBC_MS
from settings import DATABASES_SQLITE3, DATABASES_MS
import logging
from Model import BaseModel

class ShopEntry(BaseModel):  
    
    def __init__(self):

        self.shop_id = None
        self.shop_name = None
        self.user_id = None
        self.status = None
        self.showRM = None
        self.__initAttr()

    def __initAttr(self):
        sql = 'select shop_id, shop_name, user_id, status, showRM from shop'
        try:
            re = self.ldb.ExecQuery(sql, ())
            self.shop_id = re[0][0]
            self.shop_name = re[0][1]
            self.user_id = re[0][2]
            self.status = re[0][3]
            self.showRM = re[0][4]
        except Exception as e:
            self.logger.error('init shop error:' + str(e))
            return

    def update(self):
        self.__initAttr()
