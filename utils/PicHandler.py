# -*- coding: utf-8 -*-
import Image
from settings import PHOTO_HEIGHT, PHOTO_WIDTH, PHOTO_FILE_NAME_BEFORE, PHOTO_FILE_NAME_AFTER


class PicHandler():

    def __init__(self):
        pass
        
    def do(self):
        
        im = Image.open(PHOTO_FILE_NAME_BEFORE)
        w_original, h_original = im.size
        w = PHOTO_WIDTH if w_original > PHOTO_WIDTH else w_original
        
        h = int(w * 0.75)

        im.resize((w, h), Image.ANTIALIAS).save(PHOTO_FILE_NAME_AFTER)
