# -*- coding: utf-8 -*-


INSERT_SHOP = "insert into shop values (?, ?, ?, ?, ?)"
INSERT_CHECK_IN = 'insert into check_in values (?, ?, ?, ?, ?)'
INSERT_CHECK_OUT = 'insert into check_out values (?, ?, ?, ?, ?)'

INSERT_EMPLOYEE = "insert into employee values(?, ?)"

INSERT_REGIONAL_MANAGER = "insert into regional_manager values(?, ?)"

SELECT_SHOP = 'select shop_id, shop_name, showRM, user_id from shop'

SELECT_EMPLOYEE = 'select id, name from employee'

SELECT_REGIONAL_MANAGER = 'select id, name from regional_manager'

UPDATE_EMPLOYEE_INFO_TIME = 'update status set employeeInfoUpdateTime = ?'

UPDATE_SHOW_RM = 'update shop set showRM=?'