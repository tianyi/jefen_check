# -*- coding: utf-8 -*-

'''
    Init SQLite3 when the program running firstly
'''

from SQLs import *
from model.EmployeeModel import EmployeeModel
import logging
from Database import DBSQLite, ODBC_MS
from settings import DATABASES_SQLITE3, DATABASES_MS, GET_EMPLOYEEIFNO_URL
from utils import getDateTimeNow
import simplejson

# use HTTP
from MultipartPostHandler import MultipartPostHandler
import urllib2
import cookielib


INIT_DBTABASE = [

    'create table if not exists employee ( \
        ID varchar(50) primary key, \
        name varchar(50))',

    'delete from employee',

    'create table if not exists regional_manager ( \
        ID varchar(50) primary key, \
        name varchar(50))',

    'delete from regional_manager',

    'create table if not exists check_in ( \
        ID integer primary key autoincrement , \
        checkin_time DATETIME, \
        employee_id varchar(50) REFERENCES employee(id) ON DELETE CASCADE, \
        isUploaded bool, \
        check_pic blob)',

    'delete from check_in',

    'create table if not exists check_out ( \
        ID integer primary key autoincrement , \
        checkout_time DATETIME, \
        employee_id varchar(50) REFERENCES employee(id) ON DELETE CASCADE, \
        isUploaded bool, \
        check_pic blob)',

    'delete from check_out',

    'create table if not exists status ( \
        employeeInfoUpdateTime DATETIME)',

    'delete from status',

    # 'insert into status values(1, Null)',

    'create table if not exists shop ( \
        shop_id integer, \
        shop_name varchar(50), \
        user_id varchar(50), \
        status integer,\
        showRM integer)',

    'delete from shop',
]


class InitDBSQLite3():

    def __init__(self, shopID, shopName, userID, statu):
        self.logger = logging.getLogger('main.InitDBSQLite3')
        self.ldb = DBSQLite(DATABASES_SQLITE3['DB_FILE_NAME'])
        self.odbc_ms = ODBC_MS(
            DATABASES_MS['DRIVER'],
            DATABASES_MS['SERVER'],
            DATABASES_MS['DATABASE'],
            DATABASES_MS['UID'],
            DATABASES_MS['PWD']
        )
        self.shopID = shopID
        self.shopName = shopName
        self.userID = userID
        self.statu = statu
        cookies = cookielib.CookieJar()
        # import pdb; pdb.set_trace()
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies), MultipartPostHandler)
        self.__createDB()
        self.__saveShopInfo()
        self.__syncEmployeeInfo()


    def __createDB(self):
        for s in INIT_DBTABASE:
            self.ldb.ExecNoQuery(s, ())
        self.ldb.commit()
        self.logger.info('create db finished!')

    def __syncEmployeeInfo(self):

        # sql = "exec GetEmployeeList 'bj001'"
        # import pdb; pdb.set_trace()
        params = {
            "shop_id": self.shopID,
        }
        try:
            re = self.opener.open(GET_EMPLOYEEIFNO_URL, params)
            data = re.read()
            data_dic = simplejson.loads(data)
        except Exception as e:
            self.logger.error('Sync employee fail: username=' + self.shopID)
            return
        # sql = "exec GetEmployeeList '%s'" % (self.shopID)
        # try:
        #     re = self.odbc_ms.ExecQuery(sql)
        # except Exception as e:
        #     self.logger.error('Sync employee fail: sql=' + sql)
        if(data_dic["status"] == 1):
            sql = 'delete from employee'
            self.ldb.ExecQuery(sql, ())
            # import pdb; pdb.set_trace()
            for em in data_dic["data"]:
                # print r
                # "name": i.name,
                # "status": i.status,
                # "employee_id": i.employee_id,
                # "shop_id": i.shop_id,
                if(em["employee_type"] == 0):
                    self.ldb.ExecQuery(INSERT_EMPLOYEE, (em["employee_id"], em["name"]))
                elif(em["employee_type"] == 1):
                    self.ldb.ExecQuery(INSERT_REGIONAL_MANAGER, (em["employee_id"], em["name"]))
                else:
                    self.logger.error('save employee, something error:' + str(em))
            sql = 'insert into status values(?)'
            t = getDateTimeNow()
            self.ldb.ExecQuery(sql, (t, ))
            self.ldb.commit()
            self.logger.info('get employee suc. employee num=' + str(len(data_dic["data"])))
        else:
            self.logger.error('get employee info fail, get Null info')
            self.logger.error('data_dic=' + str(data_dic))


    def __saveShopInfo(self):

        sql = INSERT_SHOP
        # 0: do not show REGIONAL_MANAGER, 1 show REGIONAL_MANAGER
        self.ldb.ExecNoQuery(sql, (self.shopID, self.shopName, self.userID, self.statu, 0))
        self.ldb.commit()
        self.logger.info('save shop info finished!')




