#!/usr/bin/python
# -*- coding: utf-8 -*-


class MyException(Exception):

    def __init__(self, errorCode, msg):
        Exception.__init__(self) 
        self.errorCode = errorCode
        self.msg = msg

    def __str__(self):
        return repr(self.errorCode + ':' + self.msg)