# -*- coding: utf-8 -*

from view.LoginFrame import LoginFrame
from view.CheckFrame import CheckFrame
import wx
from utils.utils import isFirstlyRun
import logging.config
from settings import FRAME_HEIGHT, FRAME_WIDTH
import os
import sys


class MyApp(wx.App):

    def OnInit(self):

        # import pdb; pdb.set_trace()

        if(isFirstlyRun()):
            self.frame = LoginFrame(None)
            self.frame.SetMaxSize((FRAME_WIDTH, FRAME_HEIGHT))
            self.frame.SetMinSize((FRAME_WIDTH, FRAME_HEIGHT))
        else:
            self.frame = CheckFrame(None)
        self.SetTopWindow(self.frame)
        self.frame.SetMaxSize((FRAME_WIDTH, FRAME_HEIGHT))
        self.frame.SetMinSize((FRAME_WIDTH, FRAME_HEIGHT))
        self.frame.Show(True)
        return True

if __name__ == '__main__':

    logging.config.fileConfig('logging.conf')
    app = MyApp()

    # with open("name1.log", "wb") as f:
    #     for i in os.popen('tasklist').readlines():
    #         f.write(str(str(i)))

    x = [t.split()[0:1]  for t in os.popen('tasklist').readlines() if("check.exe" in t.split()[0:1]) ]

    # with open("name.log", "wb") as f:
    #     f.write(str(x))

    if(len(x) > 1):
        # f = wx.Frame()
        # f.show()
        dlg = wx.MessageDialog(None, u"考勤系统正在运行", caption='Message', style=wx.OK, pos=wx.DefaultPosition)
        dlg.ShowModal()
        sys.exit(0)

    
    app.MainLoop()

