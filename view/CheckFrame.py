# -*- coding: utf-8 -*

import wx
# import wx.xrc
import VideoCapture
from datetime import datetime
import time
from utils.PicHandler import PicHandler
from settings import PHOTO_FILE_NAME_DISPLAY, DATABASES_SQLITE3, PHOTO_FILE_NAME_BEFORE, FRAME_HEIGHT, FRAME_WIDTH
from Frame import BaseFrame
from controller.CheckController import CheckController
from model.CheckModel import CheckModel
from utils.utils import toUnicode, getDialog, datetimeToString
from DataUploader import DataUploader
from wx.lib.pubsub import setupkwargs
from wx.lib.pubsub import pub
from threading import Timer
import logging


class CheckFrame(BaseFrame):

    def __init__(self, parent):

        self.logger = logging.getLogger('main.CheckFrame')

        wx.Frame.__init__(self, parent, id=wx.ID_ANY,
                          title=u'吉芬考勤系统',
                          pos=wx.DefaultPosition,
                          size=wx.Size(FRAME_WIDTH, FRAME_HEIGHT),
                          style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)
        BaseFrame.__init__(self)
        self.controller = CheckController()
        self.cam = None
        # init GUI
        self.InitGUI()

        # register a pub
        pub.subscribe(self.update, "autoUpload")

        # join：如在一个线程B中调用threada.join()，则thread a结束后，线程B才会接着threada.join()往后运行。
        # setDaemon：主线程A启动了子线程B，调用b.setDaemaon(True)，则主线程结束时，会把子线程B也杀死，与C/C++中得默认效果是一样的。
        du = DataUploader(self.controller)

        # timerAuto = Timer(1, du.autoUploadHandler)
        # timerAuto.setDaemon(True)

        # # du.timer.start()
        du.setDaemon(True)
        du.start()
        # timerAuto.start()

    def InitGUI(self):
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        # self.m_paneLeft = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_paneLeft = wx.Panel(self, -1, size=(550, 500))
        # self.m_paneRight = wx.Panel(self, -1, size=(250, 500))
        # self.m_paneRight.SetBackgroundColour('#F5DEB3')
        self.status_text = wx.StaticText(self.m_paneLeft, -1, u"摄像头未启动", pos=(200, 100), size=(130, 50), style=wx.ALIGN_CENTRE)
        # self.status_text.Show(False)
        self.m_buttonLauncher = wx.Button(self.m_paneLeft, id=wx.ID_ANY, label=u"启动摄像头", pos=(225, 180), size=(100, 100), style=10)

        self.m_buttonCheckin = wx.Button(self, wx.ID_ANY, u"上班", size=(100, 70))
        self.m_buttonCheckin.Disable()
        self.m_buttonCheckout = wx.Button(self, wx.ID_ANY, u"下班", size=(100, 70))
        self.m_buttonCheckout.Disable()
        self.m_buttonSync = wx.Button(self, wx.ID_ANY, u"同步员工信息", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_buttonUpload = wx.Button(self, wx.ID_ANY, u"上传考勤数据", wx.DefaultPosition, wx.DefaultSize, 0)
        self.employeeName = wx.StaticText(self, -1, u"员工姓名:", pos=wx.DefaultPosition, size=wx.DefaultSize, style=wx.ALIGN_CENTER)

        self.employeeList = [i[1] for i in self.controller.getModel().getEmployeeList()]


        self.choiceEmployee = wx.Choice(self, -1, (90, 198), choices=self.employeeList, name="CellsForm")
        self.choiceEmployee.SetSelection(0)

        # import pdb; pdb.set_trace()
        updateTime = datetimeToString(self.controller.getModel().getRefreshTime())
        time = u'同步完成: ' + updateTime
        # print time
        self.employeeInfoStatus = wx.StaticText(self, -1, time, (100, 30))
        self.employeeInfoStatus.SetLabel(time)
        self.uploadStatus = wx.StaticText(self, -1, u"未知", (100, 30))

        shopName = self.controller.getModel().getShopName()

        self.shopName = wx.StaticText(self, -1, toUnicode(shopName))

        # top box
        topBox = wx.BoxSizer(wx.HORIZONTAL)
        boxLeft = wx.BoxSizer(wx.VERTICAL)
        boxLeft.Add(self.m_paneLeft)
        boxRight = wx.BoxSizer(wx.VERTICAL)

        right_border = 25

        boxRight.Add(self.shopName, 0, flag=wx.ALIGN_CENTER | wx.LEFT | wx.RIGHT | wx.TOP, border=right_border)

        box1 = wx.BoxSizer(wx.HORIZONTAL)
        box1.Add(self.employeeName, proportion=1, flag=wx.TOP | wx.BOTTOM, border=0)
        box1.Add(self.choiceEmployee, proportion=7, flag=wx.LEFT, border=10)

        boxRight.Add(box1, proportion=0, flag=wx.LEFT | wx.RIGHT | wx.TOP, border=right_border)

        box2 = wx.BoxSizer(wx.VERTICAL)
        box2.Add(self.m_buttonCheckin, proportion=0, flag=wx.TOP, border=0)
        box2.Add(self.m_buttonCheckout, proportion=0, flag=wx.TOP, border=10)

        boxRight.Add(box2, proportion=0, flag=wx.ALIGN_CENTER | wx.LEFT | wx.RIGHT | wx.TOP, border=right_border)

        boxRight.Add(wx.StaticLine(self, -1, size=(20, -1), style=wx.LI_HORIZONTAL), 0, wx.GROW | wx.ALIGN_CENTER_VERTICAL | wx.TOP, border=50)

        box3 = wx.BoxSizer(wx.HORIZONTAL)
        box3.Add(self.m_buttonUpload, proportion=0, flag=wx.TOP, border=10)
        box3.Add(self.uploadStatus, proportion=0, flag=wx.TOP | wx.LEFT | wx.BOTTOM, border=10)

        boxRight.Add(box3, proportion=1, flag=wx.ALIGN_LEFT | wx.LEFT | wx.RIGHT, border=right_border)

        box4 = wx.BoxSizer(wx.HORIZONTAL)
        box4.Add(self.m_buttonSync, proportion=0, flag=wx.TOP, border=10)
        box4.Add(self.employeeInfoStatus, proportion=0, flag=wx.TOP | wx.LEFT | wx.BOTTOM, border=10)

        boxRight.Add(box4, proportion=1, flag=wx.ALIGN_LEFT | wx.LEFT | wx.RIGHT, border=right_border)

        self.m_showRM = wx.Button(self, wx.ID_ANY, u"显示区域经理", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_hideRM = wx.Button(self, wx.ID_ANY, u"隐藏区域经理", wx.DefaultPosition, wx.DefaultSize, 0)

        box5 = wx.BoxSizer(wx.HORIZONTAL)
        box5.Add(self.m_showRM, proportion=0, flag=wx.TOP, border=10)
        box5.Add(self.m_hideRM, proportion=0, flag=wx.TOP | wx.LEFT | wx.BOTTOM, border=10)

        boxRight.Add(box5, proportion=0, flag=wx.ALIGN_LEFT | wx.LEFT | wx.RIGHT, border=right_border)

        topBox.Add(boxLeft, proportion=0, flag=wx.ALL, border=0)
        topBox.Add(boxRight, proportion=1, flag=wx.ALL, border=0)

        self.SetSizer(topBox)
        self.Centre()
        self.timer = wx.Timer(self)

        # bind envet
        self.Bind(wx.EVT_TIMER, self.OnIdel, self.timer)
        self.m_buttonCheckin.Bind(wx.EVT_BUTTON, self.Checkin)
        self.m_buttonCheckout.Bind(wx.EVT_BUTTON, self.Checkout)
        self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)
        self.m_buttonSync.Bind(wx.EVT_BUTTON, self.syncEmployeeInfo)
        self.m_buttonLauncher.Bind(wx.EVT_BUTTON, self.Launcher)
        self.m_buttonUpload.Bind(wx.EVT_BUTTON, self.manualUpload)

        self.m_showRM.Bind(wx.EVT_BUTTON, self.__showRM)
        self.m_hideRM.Bind(wx.EVT_BUTTON, self.__hideRM)

    def __showRM(self, enevt):
        self.controller.getModel().updateShowRM(1)
        self.employeeList = [i[1] for i in self.controller.getModel().getEmployeeList()]
        self.choiceEmployee.Clear()
        self.choiceEmployee.AppendItems(self.employeeList)
        self.choiceEmployee.SetSelection(0)
        self.logger.info("set show RM info = true.")

    def __hideRM(self, enevt):
        self.controller.getModel().updateShowRM(0)
        self.employeeList = [i[1] for i in self.controller.getModel().getEmployeeList()]
        self.choiceEmployee.Clear()
        self.choiceEmployee.AppendItems(self.employeeList)
        self.choiceEmployee.SetSelection(0)
        self.logger.info("set show RM info = false.")

    def update(self, msg):        
        if(msg['statu'] == 0):
            self.m_buttonUpload.Disable()  
        elif(msg['statu'] == 1 or msg['statu'] == -1):
            self.m_buttonUpload.Enable()
        self.uploadStatus.SetLabel(msg['msg'])
            
    def OnIdel(self, event):

        # pass
        # self.cam.saveSnapshot(PHOTO_FILE_NAME_DISPLAY)
        # import pdb; pdb.set_trace()
        # img = wx.Image(PHOTO_FILE_NAME_DISPLAY, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        # img = self.cam.getImage()
        pilImage = self.cam.getImage()

        image = wx.EmptyImage(pilImage.size[0], pilImage.size[1])
        image.SetData(pilImage.convert("RGB").tostring())
        image.SetAlphaData(pilImage.convert("RGBA").tostring()[3::4])

        bitmap = wx.BitmapFromImage(image)

        dc = wx.ClientDC(self.m_paneLeft)
        dc.DrawBitmap(bitmap, 0, 0, False)

    def Checkout(self, event):

        index = self.choiceEmployee.GetSelection()
        if(index == 0):
            dlg = getDialog(self, u'请选择员工名字!')
            if dlg.ShowModal() == wx.ID_YES:
                dlg.Destroy()
            return
        # import pdb; pdb.set_trace()
        employeeID, employeeName, isRM = self.controller.getModel().getEmployeeByIndex(index)
        self.logger.info("Button is click checkout: id=" + str(employeeID) + " name=" + str(employeeName.encode("utf-8")))
        self.cam.saveSnapshot(PHOTO_FILE_NAME_BEFORE)
        self.timer.Stop()
        
        statu = self.controller.getModel().saveCheckout(employeeID)

        if(statu == 1):
            message = u"%s 下班签到成功！"
            self.manualUpload(event)
            self.logger.info("check out suc: id=" + str(employeeID) + " name=" + str(employeeName.encode("utf-8")))
        elif(statu == -1):
            message = u"%s 下班签到 出错，请重试！"
            self.logger.info("check out failed: id=" + str(employeeID))

        dlg = wx.MessageDialog(self, message % (employeeName), caption='Message', style=wx.OK, pos=wx.DefaultPosition)

        if dlg.ShowModal() == wx.ID_YES:
            self.Close(True)
        dlg.Destroy()
        self.timer.Start(100)

    def Checkin(self, event):

        index = self.choiceEmployee.GetSelection()
        if(index == 0):
            dlg = getDialog(self, u'请选择员工名字!')
            if dlg.ShowModal() == wx.ID_YES:
                dlg.Destroy()
            return
        # import pdb; pdb.set_trace()
        employeeID, employeeName, isRM = self.controller.getModel().getEmployeeByIndex(index)
        self.logger.info("Button is click checkin: id=" + str(employeeID) + " name=" + str(employeeName.encode("utf-8")))
        self.cam.saveSnapshot(PHOTO_FILE_NAME_BEFORE)
        self.timer.Stop()
        
        statu = self.controller.getModel().saveCheckin(employeeID)

        if(statu == 1):
            message = u"%s 上班签到成功！"
            self.manualUpload(event)
            self.logger.info("check out suc: id=" + str(employeeID) + " name=" + str(employeeName.encode("utf-8")))
        elif(statu == -1):
            message = u"%s 上班签到 出错，请重试！"
            self.logger.info("check in failed: id=" + str(employeeID))

        dlg = wx.MessageDialog(self, message % (employeeName), caption='Message', style=wx.OK, pos=wx.DefaultPosition)

        if dlg.ShowModal() == wx.ID_YES:
            self.Close(True)
        dlg.Destroy()
        self.timer.Start(100)

    def OnCloseWindow(self, event):

        i = self.controller.getModel().ExistNotUploadData()
        if(i):
            dlg = wx.MessageDialog(None, u"有未上传签到数据，真的要退出么?", u"消息", wx.YES_NO | wx.ICON_HAND)
            if dlg.ShowModal() == wx.ID_YES:
                self.Destroy()
            dlg.Destroy()
        else:
            self.Destroy()

    def Launcher(self, event):

        self.status_text.Show(True)
        self.status_text.SetLabel(u'摄像头启动中...')
        # self.m_buttonLauncher.SetLabel(u'摄像头启动中...')
        self.m_buttonLauncher.Disable()
        try:
            self.cam = VideoCapture.Device()
            self.m_buttonLauncher.Show(False)

        except:
            self.status_text.SetLabel(u'摄像头启动出错,请检查摄像头,然后重试！')
            # self.m_buttonLauncher.SetLabel(u'<html>摄像头启动出错,请检查摄像头！点击重试</html>')
            self.m_buttonLauncher.Enable()
            return
        self.status_text.Show(False)
        # self.cam.saveSnapshot(PHOTO_FILE_NAME_DISPLAY)
        self.timer.Start(100)
        event.Skip()
        self.m_buttonCheckin.Enable()
        self.m_buttonCheckout.Enable()

    def syncEmployeeInfo(self, event):
        # import pdb; pdb.set_trace()
        progressMax = 100
        dialog = wx.ProgressDialog(u"信息", u"正在连接服务器", progressMax)
        # self.choiceEmployee.Clear()
        dialog.Update(10, u'正在同步数据')
        self.controller.getModel().sync()
        dialog.Update(50, u'员工数据获取成功')
        self.employeeList = [i[1] for i in self.controller.getModel().getEmployeeList()]
        dialog.Update(70, u'更新数据库')
        self.choiceEmployee.Clear()
        self.choiceEmployee.AppendItems(self.employeeList)
        self.choiceEmployee.SetSelection(0)
        updateTime = self.controller.getModel().getRefreshTime()
        self.employeeInfoStatus.SetLabel(u'同步完成: ' + updateTime.__str__())
        dialog.Update(100, u'同步完成')
        dialog.Destroy()

    def manualUpload(self, event):

        """ 
        Send Message to DataUploader 
        """  
        # pub.sendMessage("update", msg={'data': 1, 'msg': 'asdfasdfasdfasd'})
        # wx.CallAfter()
        self.uploadStatus.SetLabel(u'手工数据上传中')
        # msg = {'data': 1, 'msg': 'from GUI'}
        pub.sendMessage("manualUpload")
        # print datetime.now()

        # self.choiceEmployee.AppendItems(li)

