# -*- coding: utf-8 -*-

import wx
from settings import DATABASES_SQLITE3
import time
from Frame import BaseFrame
from controller.LoginController import LoginController
from model.LoginModel import LoginModel
from CheckFrame import CheckFrame
from utils.utils import getDialog


class LoginFrame(BaseFrame):

    def __init__(self, parent):

        BaseFrame.__init__(self)
        m = LoginModel()
        self.controller = LoginController(m)
        self.InitGUI(parent)

    def InitGUI(self, parent):

        # wx.Frame.__init__(self, parent, id=-1, title='One', size=(300, 200))
        wx.Frame.__init__(self, parent, id=wx.ID_ANY,
                          title=u'欢迎登陆吉芬考勤系统',
                          pos=wx.DefaultPosition,
                          size=wx.Size(800, 500),
                          style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)
        panel = wx.Panel(self)
        login = wx.Button(parent=panel, id=-1, label=u'登录', pos=(30, 150))
        cancel = wx.Button(parent=panel, id=-1, label=u'取消', pos=(180, 150))
        wx.StaticText(panel, -1, u"操作员代码", (30, 35))
        wx.StaticText(panel, -1, u"密      码", (30, 95))
        self.Username = wx.TextCtrl(panel, -1, "", (100, 30), (175, - 1))
        self.Password = wx.TextCtrl(panel, -1, "", (100, 90), (175, -1), wx.TE_PASSWORD)
        self.Bind(wx.EVT_BUTTON, self.Login, login)
        self.Bind(wx.EVT_BUTTON, self.LogOut, cancel)

    def Login(self, evt):
        UserName = self.Username.GetValue()
        PassWord = self.Password.GetValue()
        # import pdb; pdb.set_trace()
        f = {"status": -3}
        if(not UserName or not PassWord):
            dlg = getDialog(self, msg=u'请输入用户名和密码!')
            if dlg.ShowModal() == wx.ID_YES:
                dlg.Destroy()
            return
        else:
            f = self.controller.ValidateLogin(UserName, PassWord)
            # "status": 1, 
            # "msg": "successful", 
            # "username": u.username, 
            # "usertype": u.user_type,
            # "shop_name": u.shop_name,
            # "shop_status": u.shop_status,
        if(f["status"] == 1):
            progressMax = 100
            dialog = wx.ProgressDialog(u"信息", u"正在连接服务器", progressMax)
            time.sleep(1)
            dialog.Update(50, u'正在初始化...')
            self.controller.InitSQLite3(
                shopID=f["shop_id"], 
                shopName=f["shop_name"], 
                userID=f["username"], 
                statu=f["shop_status"],
            )
            time.sleep(1)
            dialog.Update(100, u'初始化完成!')
            time.sleep(1)
            dialog.Destroy()
            self.Destroy()
            m = CheckFrame(None)
            
            m.Show(True)
        elif(f["status"] == -1):
            dlg = getDialog(self, msg=u'用户名或者密码错误!')
            if dlg.ShowModal() == wx.ID_YES:
                self.Close(True)
                dlg.Destroy()
            return
        else:
            dlg = wx.MessageDialog(self, u'未知错误，请联系管理员或者重试! Error Code=' + str(f), caption='Message', style=wx.OK | wx.ICON_HAND, pos=wx.DefaultPosition)
            if dlg.ShowModal() == wx.ID_YES:
                self.Close(True)
            dlg.Destroy()

    def LogOut(self, evt):
        self.Destroy()

    
