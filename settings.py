# -*- coding: utf-8 -*

PHOTO_FILE_NAME_BEFORE = 'jefenbefore.jpg'
PHOTO_FILE_NAME_AFTER = 'jefenafter.jpg'
PHOTO_FILE_NAME_DISPLAY = 'jefendisplay.jpg'
PHOTO_FILE_NAME_UPLOAD = 'jefenupload.jpg'

PHOTO_WIDTH = 400
PHOTO_HEIGHT = 300

FRAME_WIDTH = 910
FRAME_HEIGHT = 500

EMPLOYEE_NO_BODY = u'请选择'

# days
LOCAL_DATA_EXPIRE = 60

# second
TIME_TO_UPLOAD = 60


# db setting for MS SQL server

# DRIVER=‘{SQL Server}'这个一般是固定的，除非你在Sql Server作了更改。
# SERVER：此参数为数据库服务器名称，不是"192.168.0.X"这种，一般在安装时命名好了，我的是：ZHANGHUAMIN\MSSQLSERVER_ZHM
# DATABASE：此参数指的是Sql Server内具体的数据库了，使用这个connect接口连接之前在sqlserver内应该是已经先创建好并存在的，否则连接不上。
# UID：用户名
# PWD：密码

DATABASES_MS = {
    'DRIVER': 'SQL SERVER',
    'SERVER': '192.168.0.243',
    'DATABASE': 'CheckInOut',
    'UID': 'sa',
    'PWD': 'NewSql2014-11-20'
}

# db setting for SQLite3

DATABASES_SQLITE3 = {
    'DB_FILE_NAME': 'PG8JgPfwsq6KLBkhoGitXB.mx'
}


UPLOAD_PIC_URL_IN    = 'http://tg.jefen.com:90/check/uploadImageInfoCheckin/'
UPLOAD_PIC_URL_OUT   = 'http://tg.jefen.com:90/check/uploadImageInfoCheckout/'
LOGIN_VALIDATOIN_URL = 'http://tg.jefen.com:90/check/loginValidation/'
GET_EMPLOYEEIFNO_URL = 'http://tg.jefen.com:90/check/getEmployeesInfo/'
UPLOAD_USER_NAME = 'admin'
UPLOAD_PASSWORK = '123456'

# UPLOAD_PIC_URL_IN    = 'http://192.192.192.101:8000/check/uploadImageInfoCheckin/'
# UPLOAD_PIC_URL_OUT   = 'http://192.192.192.101:8000/check/uploadImageInfoCheckout/'
# LOGIN_VALIDATOIN_URL = 'http://192.192.192.101:8000/check/loginValidation/'
# GET_EMPLOYEEIFNO_URL = 'http://192.192.192.101:8000/check/getEmployeesInfo/'
